const webpush = require('web-push'),
    express = require('express'),
    cors = require('cors'),
    bodyParser = require('body-parser'),
    fs = require('fs'),
    Pusher = require('pusher');

var host = "https://pwa.hackevolve.com"
var serverHost = "https://pwa-server.hackevolve.com";

var channels_client = new Pusher({
    appId: '894443',
    key: '08f8a0bb1c7c3abdb57d',
    secret: '120dbedbc308323a1880',
    cluster: 'ap2',
    encrypted: true
});

const rawData = fs.readFileSync('../seed.json')
let submissions = JSON.parse(rawData);

// run `web-push generate-vapid-keys` command to get keys.
webpush.setVapidDetails(
    'mailto:saideeptalari@gmail.com',
    'BApmPOUoPGduVRNVjnKrv0cn3WgId9kg3hqoVDYg4NJZIf6mXSHfpfMAzv5-v7uFIwVXPOK7plhPUv8_xeku-Js',
    'ZSRFFOGr581R0OOz8xutZzVfqFqC1qYSpeByFAX351w'
);

const app = express();
app.use(bodyParser.json())
app.use(cors());

let allSubscriptions = new Set();

function registerPushSubscription(data) {
    const pushSubscription = {
        endpoint: data.endpoint,
        keys: {
            auth: data.authSecret,
            p256dh: data.key
        }
    }
    return pushSubscription;
}

function sendNotification(subscription, data) {
    const iconUrl = `${host}/public/images/logo/upvote.png`;

    return webpush.sendNotification(subscription, JSON.stringify({
        msg: data.message,
        url: `${host}`,
        icon: iconUrl
    }))
}

function broadcastNotification(data) {
    for (let subscription of allSubscriptions) {
        sendNotification(subscription, data)
    }
}

app.post('/register', async (req, res) => {
    let subscription = registerPushSubscription(req.body);
    allSubscriptions.add(subscription);

    try {
        result = await sendNotification(subscription, { message: "You will now receive push notifications!" });
        res.sendStatus(201);
    } catch (err) {
        console.log(err)
    }
})

app.post('/api/notify', (req, res) => {
    broadcastNotification(req.body)
    res.sendStatus(201)
})

app.get('/api/submissions', (req, res) => {
    res.send(submissions);
})

app.post('/api/upvote', (req, res) => {
    const submission = submissions.find(sub => sub.id == req.body.id);
    submission.votes++;

    channels_client.trigger('upvote', 'vote', {
        "event": "upvote",
        "submission": submission
    });
    res.sendStatus(200);
})

app.listen(8080, () => console.log("Server listening on :8080"))