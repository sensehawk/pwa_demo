var host = "https://pwa.hackevolve.com";
var serverHost = "https://pwa-server.hackevolve.com"

function urlBase64ToUint8Array(base64String) {                      3
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
      .replace(/\-/g, '+')
      .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
  }


var endpoint;
var key;
var authSecret;
var vapidPublicKey = "BApmPOUoPGduVRNVjnKrv0cn3WgId9kg3hqoVDYg4NJZIf6mXSHfpfMAzv5-v7uFIwVXPOK7plhPUv8_xeku-Js";

async function registerPushNotifications(swRegistration) {
    return swRegistration.pushManager.getSubscription().then(subscription => {
        if (subscription) return subscription;
        return swRegistration.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: urlBase64ToUint8Array(vapidPublicKey)
        }).then(subscription => {
            var rawKey = subscription.getKey ? subscription.getKey('p256dh') : '';
            key = rawKey ? btoa(String.fromCharCode.apply(null, new Uint8Array(rawKey))) : '';
            var rawAuthSecret = subscription.getKey ? subscription.getKey('auth') : '';
            authSecret = rawAuthSecret ? btoa(String.fromCharCode.apply(null, new Uint8Array(rawAuthSecret))) : '';
            endpoint = subscription.endpoint;

            return fetch(`${serverHost}/register`, {
                method: 'post',
                headers: new Headers({
                    'content-type': 'application/json'
                }),
                body: JSON.stringify({
                    endpoint: subscription.endpoint,
                    key: key,
                    authSecret: authSecret
                })
            })
        })
    }).catch(err => {
        console.log("Push notification subscription declined/failed!")
    })
}