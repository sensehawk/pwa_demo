importScripts("https://js.pusher.com/5.0/pusher.worker.min.js")
importScripts('https://cdn.jsdelivr.net/npm/idb-keyval@3/dist/idb-keyval-iife.min.js');

// event.waitUntil will help the service worker to be active till 
// the promise passed to the method will either resolve/reject.
// Otherwise it will simply continue with state change which is not desired.
// https://www.w3.org/TR/service-workers/#service-worker-lifetime

var host = "https://pwa.hackevolve.com";
var serverHost = "https://pwa-server.hackevolve.com";

var preCacheName = "UpVote-precache";
var staticCacheName = "UpVote-static";
var apiCacheName = "UpVote-api";

self.addEventListener('install', event => {
    console.log("Installing service worker");
    event.waitUntil(
        caches.open(preCacheName).then(cache => {
            cache.addAll([
                "https://unpkg.com/vue",
                "/public/styles.css",
            ])
            self.skipWaiting()
        })
    )
})

self.addEventListener('activate', event => {
    console.log("Activated service worker");
    event.waitUntil(self.clients.claim())
})

// self.addEventListener('fetch', event => {
//     // NOTE: caches.match(obj) does not throw an error when the object doesn't match
//     // it will simply be undefined.
//     event.respondWith(caches.match(event.request).then(response => {
//         if (response) {
//             console.log("[+] Serving from cache:", event.request.url);
//             return response
//         };
//         if (/\.json$/.test(event.request.url)) {
//             var requestToCache = event.request.clone();
//             return fetch(event.request).then(response => {
//                 var responseToCache = response.clone();
//                 caches.open(apiCacheName).then(cache => {
//                     cache.put(requestToCache, responseToCache);
//                 })
//                 return response;
//             }).catch(err => {
//                 console.log("[-] Error fetching resource:", err)
//             })
//         }
//         return fetch(event.request);
//     }))
// })


self.addEventListener('fetch', event => {
    if (event.request.method !== "GET") return event.respondWith(fetch(event.request))
    event.respondWith(caches.match(event.request).then(cachedResponse => {
        if ((! /.*\/api\/.*/.test(event.request.url)) && (! /.*\/main.js.*/.test(event.request.url))) {
            if (cachedResponse) {
                console.log("[+] Serving from cache:", event.request.url)
                return cachedResponse;
            }
            var requestToCache = event.request.clone();
            return fetch(event.request).then(response => {
                var responseToCache = response.clone();
                if (response.status === 200) {
                    caches.open(staticCacheName).then(cache => {
                        cache.put(requestToCache, responseToCache)
                    })
                }
                return response;
            })
        } else {
            var requestToCache = event.request.clone();
            return fetch(event.request).then(response => {
                var responseToCache = response.clone();
                if (response.status === 200) {
                    caches.open(apiCacheName).then(cache => {
                        cache.put(requestToCache, responseToCache);
                    })
                    return response;
                } else if (cachedResponse) return cachedResponse;
            }).catch(err => {
                if (cachedResponse) return cachedResponse;
                console.log("[-] NEW Error fetching resource:", err)
                return new Response("Couldn't reach server", { "status": 502 })
            })
        }
    }))
})

function sendMessageToClients(message) {
    self.clients.matchAll().then(clients => {
        clients.forEach(client => {
            client.postMessage(message)
        })
    })
}

self.addEventListener('message', message => {
    // console.log("[->] Event:", message.data.event)
    // console.log("[+] Submission:", message.data.submission)
    sendMessageToClients(message.data);
})

self.addEventListener('push', (event) => {
    let payload = event.data ? JSON.parse(event.data.text()) : 'no payload';
    let title = "UpVote Notification";

    event.waitUntil(self.registration.showNotification(title, {
        body: payload.msg,
        url: payload.url,
        icon: payload.icon,
        actions: [
            { action: 'like', title: '👍Like' },
            { action: 'reply', title: '⤻ Reply' }]
    }))

})

self.addEventListener('notificationclick', (event) => {
    event.notification.close();
    event.waitUntil(clients.matchAll({
        type: "window"
    }).then(clientList => {
        for (let client of clientList) {
            if (client.url == 'https://pwa.hackevolve.com/' && 'focus' in client) return client.focus();
        }
        if (clients.openWindow) {
            return clients.openWindow('/')
        }
    }))
})

var pusher = new Pusher('08f8a0bb1c7c3abdb57d', {
    cluster: 'ap2',
    forceTLS: true
});

var channel = pusher.subscribe('upvote');
channel.bind('vote', function (data) {
    sendMessageToClients(data);
});

self.addEventListener('sync', event => {
    if (event.tag.includes('upvote')) {
        uid = event.tag.split("-")[1]
        submissionId = `submission-${uid}`
        console.log("Recieved sync event with uid :", submissionId)
        event.waitUntil(
            idbKeyval.get(submissionId).then(submission => {
                if (submission) {
                    fetch(`${serverHost}/api/upvote`, {
                        method: 'POST', 
                        body: JSON.stringify(submission),
                        headers: {'Content-Type': "application/json"}
                    }).then(res => {
                        idbKeyval.del(submissionId);
                    })
                }
            })
        )
    }
})