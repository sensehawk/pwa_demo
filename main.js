var host = "https://pwa.hackevolve.com";
var serverHost = "https://pwa-server.hackevolve.com"

const submissionComponent = {
  template:
    ` <div style="display: flex; width: 100%">
      <figure class="media-left">
        <img class="image is-64x64"
          :src="submission.submissionImage">
      </figure>
      <div class="media-content">
        <div class="content">
          <p>
            <strong>
              <a :href="submission.url" class="has-text-info">
                {{ submission.title }}
              </a>
              <span class="tag is-small">#{{ submission.id }}</span>
            </strong>
            <br>
              {{ submission.description }}
            <br>
            <small class="is-size-7">
              Submitted by:
              <img class="image is-24x24"
                :src="submission.avatar">
            </small>
          </p>
        </div>
      </div>
      <div class="media-right">
        <span class="icon is-small" @click="upvote(submission.id)">
          <i class="fa fa-chevron-up"></i>
          <strong class="has-text-info">{{ submission.votes }}</strong>
        </span>
      </div>
    </div>`,
  props: ['submission', 'submissions'],
  computed: {
    async registration() {
      if ('serviceWorker' in navigator && 'SyncManager' in window) return await navigator.serviceWorker.ready;
      return null;

    }
  },
  methods: {
    makeid(length) {
      var result           = '';
      var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var charactersLength = characters.length;
      for ( var i = 0; i < length; i++ ) {
         result += characters.charAt(Math.floor(Math.random() * charactersLength));
      }
      return result;
   },
    async upvote(submissionId) {
      registration = await this.registration;

      const submission = this.submissions.find(
        submission => submission.id === submissionId
      );

      if (registration) {
        uid = this.makeid(5)
        await idbKeyval.set(`submission-${uid}`, submission);
        await registration.sync.register(`upvote-${uid}`)
      } else axios.post(`${serverHost}/api/upvote`, { "id": submissionId })
      
      submission.votes++;
    }
  }
};

new Vue({
  el: '#app',
  data: {
    submissions: []
  },
  computed: {
    sortedSubmissions() {
      return this.submissions.sort((a, b) => {
        return b.votes - a.votes
      });
    }
  },
  components: {
    'submission-component': submissionComponent
  },
  created() {
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.addEventListener('message', message => {
        // console.log("[<-] Event:", message.data.event);
        // console.log("[+] Submission:", message.data.submission);
        if (message.data.event == "upvote") {
          const submissionIdx = this.submissions.findIndex(submission => submission.id === message.data.submission.id);
          this.submissions[submissionIdx] = message.data.submission;

          // Vue will not detect the changes unless you explicitly change the object.
          this.submissions = Object.assign([], this.submissions);
        }
      })
    }
    axios.get(`${serverHost}/api/submissions`).then(({ data }) => this.submissions = data);
  }
});
